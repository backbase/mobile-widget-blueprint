### v1.0.7 - `26/08/2015, 2:57pm`
#### add tag to info.json for styleguide filtering  
* add tag to info.json for styleguide menu filtering  


### v1.0.5 - `20/08/2015, 2:05pm`
#### Fix relative path.  
* Recompile with correct relative path the theme.  


### v1.0.4 - `20/08/2015, 2:04pm`
* Recompile with correct relative path the theme.  
* Made theme a dependency, instead of dev-dependency.  
* Change bower name.  


### v1.0.3 - `20/08/2015, 12:46pm`
* Made theme a dependency, instead of dev-dependency.  
* Change bower name.  

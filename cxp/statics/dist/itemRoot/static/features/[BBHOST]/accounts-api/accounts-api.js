(function(window, angular) {
	"use strict;"

	// Accounts List Api Service
	
	var api = angular.module('api', []);
	api.factory('accountsListApi', ['$http',
		function($http) {

			var ACCOUNTS_LIST_ENDPOINT_URL = "https://dl.dropboxusercontent.com/u/18618/accounts.json";

			var service = {
				getAccountsList: getAccountsList
			};

			return service;

			function getAccountsList(additionalParams) {
				return $http.get(ACCOUNTS_LIST_ENDPOINT_URL).then(function(response) {
					return response.data;
				});
			}
		}
	]);
}(window, angular));
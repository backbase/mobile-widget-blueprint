### v2.11.1 - `01/09/2015, 6:11pm`
* ignore 401 and 403 status from being notifiable  


### v2.11.0 - `01/09/2015, 2:31pm`
* LF-177 Added contextId parameter to retryObject for notification grouping.  
* LF-177 HTTP interceptor method for configuring notification and retry queue behavior. Documentation updated.  


### v2.10.3 - `31/08/2015, 10:33am`
* add missing module accounts properties  


### v2.10.2 - `26/08/2015, 4:19pm`
* add tag to info.json for styleguide menu filtering  
* bugfix/LF-241-new-transfer-widget-all-the-accounts: - new validation function is added into core/utils/is (it is moved from         _migration)  
* Add angular helper methods
* adding i18n menu instructions for client portal  
* bugfix - remove util deps, detect localstorage suport  
* remove util global from migration some files  
* LF-211: Fix model.xml format.  
* LF-211: Add model.xml for feature definition.  
* Remove stash repository from bower.json  
* add defaultLandingPage property to lpPortal  
* NOJIRA: Add function to check if string is valid UUID  
* LPMAINT-8 URL decode lpTemplate src path.  
* Fix problem with hardcoded value of url to use setConfig  


### v2.10.1 - `26/08/2015, 2:57pm`
#### add tag to info.json for styleguide filtering  
* add tag to info.json for styleguide menu filtering  


### v2.10.0 - `25/08/2015, 11:19am`
* Add angular helper methods  
* adding i18n menu instructions for client portal  


### v2.9.6 - `12/08/2015, 1:25pm`
* bugfix - remove util deps, detect localstorage suport  
* remove util global from migration some files  


### v2.9.5 - `12/08/2015, 12:01pm`
* remove util global from migration some files  


### v2.9.4 - `11/08/2015, 5:41pm`
#### Fix model.xml format.  
* LF-211: Add model.xml for feature definition.  


### v2.9.3 - `11/08/2015, 1:38pm`
#### Add model.xml for feature definition.  


### v2.9.2 - `10/08/2015, 6:05pm`
#### Remove repository from bower.json  
* Add distribution file.  
* Add code review suggestions.  
* Fix problem with hardcoded value of url to use setConfig  


### v2.9.1 - `07/08/2015, 2:46pm`
* add defaultLandingPage property to lpPortal  
* Rebuild dist  
* NOJIRA: Bump version no  
* NOJIRA: clean up unit tests  
* NOJIRA: Add function to check if string is valid UUID  
* LPMAINT-8 URL decode lpTemplate src path.  
* LF-136 fix exception if there is no portalView  
* fixed i18n locale event order (it was a 'pub' before a 'sub')  


### v2.9.0 - `31/07/2015, 10:44am`
* NOJIRA: Add function to check if string is valid UUID


##v 2.5.0
- updated documentation

##v 2.3.1
- adding **lpCoreError.assert** method

##v 2.7.0
- removing element-resize directive (but it has moved to UI 2.4.0).

##v 2.8.4
- LF-136 fix exception if there is no portalView

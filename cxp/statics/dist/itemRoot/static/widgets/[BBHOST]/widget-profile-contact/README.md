# Profile Contact

## Information

| name                  | version           | bundle           |
| ----------------------|:-----------------:| ----------------:|
| widget-profile-contact    | 2.1.3 			| Universal        |

## Brief Description

Provides an easy way for user to update his contact information. All the controls are created using the Editable Controls.

## Dependencies

* base
* core
* ui

## Dev Dependencies

* angular-mocks ~1.2.28
* config

## Screenshots
<img src="docs/media/screenshot.png" width="50%" title="Widget Screenshot" />

## Preferences

_This widget does not have any preferences._


##Events

_This widget does not subscribe/publish to any events._


## Test

## Build

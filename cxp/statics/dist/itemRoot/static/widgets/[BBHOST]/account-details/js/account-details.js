(function(window, angular) {
    "use strict";

    //Getting data from dropbox to simulate an external resource with consistency   
    var ACCOUNT_DETAILS_ENDPOINT_URL = "https://dl.dropboxusercontent.com/u/18618/bp-stubs/account-";

    var app = angular.module('account-details', ['api']);


    app.run(function(widget) {   
        gadgets.pubsub.publish('cxp.item.loaded', {
            id: widget.model.name
        });
    });

    app.controller('main', ['$scope','$window','$log', 'widget', 'accountDetailsApi',
        function($scope, $window, $log ,widget, accountDetailsApi) {

            $scope.data = {};
           
            $window.gadgets.pubsub.subscribe("Account Details" ,function(ev){
            	console.log(ev)	
            	loadAccountDetails(ACCOUNT_DETAILS_ENDPOINT_URL + ev.id + '.json')	
            });

            function loadAccountDetails(endpoint) {
                return accountDetailsApi.getAccountDetails(endpoint)
                    .then(function (data) {
               
                        $scope.data = data;
                        
                        $log.info($scope.data);

                        if (data.length === 0) {
                            $window.gadgets.pubsub.publish(widget.getPreference('noAccountEventName'));
                        }
                    }, function (error) {
                        $log.error('Account detail loading failed', error);
              
                });
            }
        }
    ]);

}(window, angular));
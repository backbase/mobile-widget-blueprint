
Authentication module

# Information

| name                  | version       | bundle     |
| ----------------------|:-------------:| ----------:|
| module.users          | 2.2.8         | launchpad  |

## Dependencies
* base
* core

## Dev Dependencies
* angular-mocks ~1.2.28
* config


## Install

```bash
bower i module-users --save && bower link
```

## Develop

```bash
git clone ssh://git@stash.backbase.com:7999/lpm/module-users.git && cd module-users
bower install
bower link
```

## Usage

-- TO BE ADDED


## Testing

```
bb test
```

with watch flag
```
bb test -w
```

## Build

```
bb build
```



[base-url]:http://stash.backbase.com:7990/projects/lpm/repos/foundation-base/browse/
[core-url]: http://stash.backbase.com:7990/projects/lpm/repos/foundation-core/browse/
[ui-url]: http://stash.backbase.com:7990/projects/lpm/repos/ui/browse/
[config-url]: https://stash.backbase.com/projects/LP/repos/config/browse
[api-url]:http://stash.backbase.com:7990/projects/LPM/repos/api/browse/
[angular-mocks]:https://github.com/angular/bower-angular-mocks

define(function(require, exports, module) {
    'use strict';

    exports.widgetPrefs = {
        NOTIFICATIONS_ENDPOINT: 'notificationsEndpoint',
        CLOSE_NOTIFICATION_ENDPOINT: 'closeNotificationEndpoint',
        POLL_INTERVAL: 'pollInterval',
        ALLOW_PUBSUB: 'allowPubsub'
    };

});

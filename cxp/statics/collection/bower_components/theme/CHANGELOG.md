### v3.0.2 - `27/08/2015, 2:26pm`
* NGUSEOLB-377: Fix modal position when page is scrolled  


### v3.0.1 - `26/08/2015, 2:57pm`
#### add tag to info.json for styleguide filtering  
* add tag to info.json for styleguide menu filtering  


### v3.0.0 - `19/08/2015, 3:13pm`
* LF-233: Update build for new CLI tooling.  
* LF-233: Committing built banking and universal CSS.  
* LF-233: Move less dir to styles.  
* LF-233: Removing themes.  
* LF-233: Add entry points to theme, and compile default CSS.  
* LF-211: Fix model.xml format.  


### v2.2.3 - `11/08/2015, 5:41pm`
#### Fix model.xml format.  
* LF-211: Add model.xml for feature definition.  


### v2.2.2 - `11/08/2015, 1:38pm`
#### Add model.xml for feature definition.  


### v2.2.1 - `10/08/2015, 6:05pm`
#### Remove repository from bower.json  


### v2.2.0 - `30/07/2015, 3:27pm`
* NGUSEOLB-197: Fix backdrop visibility. Add centered modal dialog.  


## [2.1.0]
 - Add Backbase Neue font icons

## [1.0.0]
 - Initial release

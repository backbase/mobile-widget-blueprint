
# Bundler

Frontend dependency management for CXP.

 - Builds static assets from bower into target.
 - Copies assets into suitable locations for CXP compatiblity:
    - widgets/...
    - features/...
    - containers/...
    - pages/...

## Versioning

All static assets widgets/features/containers/pages use semantic versioning. The versions
are defined in bower.json, as well as the versions of the dependencies. This could be
switched to pacakge.json (and npm) when NPM supports a flat dependency structure like bower does.

When a bundle of assets is stable it's possible to give each asset a single version, for
example Launchpad will tag stable static assets release every two weeks with LP-0.12.X.

The version is specified in the bundles pom.xml. At build time this version is filtered into
the bower.json (and copied into the ${project.build} directory). All assets are installed
via 'bower install' into ${project.build}/bower_components.

For packaging, the assets are copied into sub directories (widgets/..., features/..., etc) 
using maven ant plugin.



### v1.2.6 - `26/08/2015, 2:57pm`
#### add tag to info.json for styleguide filtering  
* add tag to info.json for styleguide menu filtering  


### v1.2.4 - `11/08/2015, 5:41pm`
#### Fix model.xml format.  


### v1.2.3 - `11/08/2015, 1:38pm`
#### Add model.xml for feature definition.  


### v1.2.2 - `10/08/2015, 6:05pm`
#### Remove repository from bower.json  



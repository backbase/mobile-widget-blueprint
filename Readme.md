# Mobile widget blue-print iOS app


#####This app will use de blue print proposal widget for evaluation and testing

Generated from the [mobile-template-ios](https://bitbucket.org/backbase/mobile-template-ios)

WEB resources live on /www and copied to the app resources folder on build.
On Xcode this is done by running with "rsync -rtvu --delete ./../www/ www/assets/backbase/static/www/"

#####Debugging 
If the widget src is set to remote(served from a server) you can achieve an easy debug workflow.
Start the app in the simulator, connect safari developer tools, Cxp.reload() to reload the webview. No need to build on every change.


#####Contents
- /cxp - CXP 5.6 server setup.Includes MBAAS and CORS for serving widgets to remote requesters, like a mobile app;
- /ios - iOS app;
- /www - web resources, the collection; 


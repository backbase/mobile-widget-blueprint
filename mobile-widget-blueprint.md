#MOBILE WIDGET GUIDELINES

Ref:[Omni-channel Widget Development Guidelines](https://backbase.atlassian.net/wiki/pages/viewpage.action?pageId=152829978)

##NFR

- Supportability | Full product lifecycle support towards dependents, internal and external developers.
- Extendibility | Extension points to support use cases where deviations through inheritance, injection, overrides are made. Clear separation of project specific and product deliverables.
- Portability | Compatibility between release including release policies on backwards compatibility, deprecation, retirement, ..
- Compatibility | Keeping dependancies current, make sure that upgrades of dependents don’t (always) lead to need for upgrade of FE frameworks and vice versa, having dependency checks build in. Also related to versioning; have current version in maintenance mode, while working on new releases where enhancement are being carried out. Versus only having one version, which is the next version where is worked on.
- Deployability | App Store requirements, how to deploy changes.
- Modularity | Ability to decompose and isolate (versions) of modules with all it’s dependents


##NFR (Technical methodology)

###HTML

- Should be as simple as possible - premature, not globally accepted(yet) technologies or standards should be avoided at all cost;
- Readiness - Should be ready as soon as possible, the first view should never be in an external template;
- - In native apps we don't have "view jumps" - if you element will take a time to load(is getting data etc) have a placeholder for it - we can see this pattern in the iOS facebook app and many others;
- Only use standard HTML controls, use a "select" tag instead of a styled "ul > li";
- The DOM API is synchronous. If you are not using Angular.js make sure you are making DOM manipulations properly;
- If your widget has multiple panels use templates, but make sure these are loaded before the user request it, e.g. use ngShow instead ngIf, or a combination of the two to achieve panel preloading.Be Smart;
- If you have a component approach make sure the html for it is declared inline. Managing and inserting components in the DOM with JS is expensive;
- In case your component approach is using Angular directives have the template inline in the directive. 
- If your component needs multiple templates and is getting the template value from a widget preference always have a default one; 


###CSS

Proper CSS is an essential performance factor, specially on mobile. More css, more time to be parsed, complex css or complex css selectors take also more time to be applied.

- **Avoid absolute** units at all cost, use "vh/vw", "em/rem" or the good old "%". "px", "pt" etc need a very good reason to be used;
- Make sure your **theme is minimal** and only contains css that will be used;
- The widget should contain enough css to make it portable and functional without the theme;
- If you are using a component the css for it should be in the widget css, again, to make it portable and functional without relying on the theme;
- You can use a css pre-processor, be careful because with this you risk to have your **css out of control**;
- Write mobile rules outside media queries, from there start applying rules to bigger devices using media queries;
- Be careful with **flex box**, not all android implementations support it, if you use it have other techniques as fallback;
- For now our sdk is only compatible with **devices using webkit**, still avoid relying on features only present in webkit an not in other. Think in the future and universal css;
- If you need to animate an element enable **GPU acceleration** by applying "transform: translateZ(0);"
- If you have a component approach make sure the CSS dependencies are in the widget CSS, again to make sure the widget portable;

###JS

- Widget dependencies should be managed using CXP capabilities, mainly script tags in the widget head;
- Must have minimal codebase, people will build on top;
- If the widgets are angular there is a reason use require?
- Widgets should rely on the documented capability to inject scripts;
- Je should do just what it need to do;
- Functional programming is awesome, a function that returns another function that returns another isn't;
- The widget object must stay pure;
- We can't have more then 500kb before widgets. Not acceptable;
- These widgets need to run in the mobile sdk, and web everywhere;
- Our customer base tends to have medium web skills, code for them. They are the customer;
- Every included lib should have a very good reason for it. It should represent a huge gain in productivity, not just a little or not just because the developer likes it;
- Cache isn't a tool, is an helper. Never think cache first;
- Same goes for concat and minifi;


###Current proposals
[simplified-web-components-via-dom-class](https://www.webreflection.co.uk/blog/2015/09/17/simplified-web-components-via-dom-class)

[WebReflection](https://github.com/WebReflection/dom-class)


















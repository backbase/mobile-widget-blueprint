(function(window, angular) {
	"use strict;"

	// Accounts List Api Service

	var api = angular.module('api', []);
	api.factory('accountDetailsApi', ['$http',
		function($http) {
	
			var service = {
				getAccountDetails: getAccountDetails
			};

			return service;

			function getAccountDetails(endpoint) {
				return $http.get(endpoint).then(function(response) {
					return response.data;
				});
			}
		}
	]);
	
}(window, angular));
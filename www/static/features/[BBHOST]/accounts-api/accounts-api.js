(function(window, angular) {
	"use strict;"

	// Accounts List Api Service

	var api = angular.module('api', []);
	api.factory('accountsListApi', ['$http',
		function($http) {
	
			var service = {
				getAccountsList: getAccountsList
			};

			return service;

			function getAccountsList(endpoint) {
				return $http.get(endpoint).then(function(response) {
					return response.data;
				});
			}
		}
	]);
	
}(window, angular));
(function(window, angular) {
    "use strict";

    //Getting data from dropbox to simulate an external resource with consistency   
    var ACCOUNTS_LIST_ENDPOINT_URL = "https://dl.dropboxusercontent.com/u/18618/bp-stubs/accounts.json";

    var app = angular.module('accounts', ['api', 'ngTouch']);


    app.run(function(widget) {   
        gadgets.pubsub.publish('cxp.item.loaded', {
            id: widget.model.name
        });
    });

    app.controller('main', ['$scope','$window','$log', 'widget','accountsListApi',
        function($scope, $window, $log ,widget,accountsListApi) {

            $scope.data = {};
            loadAccounts(ACCOUNTS_LIST_ENDPOINT_URL);

            $scope.accountDetail = function(id){
                $log.info(id)
                $window.gadgets.pubsub.publish("Account Details",{id:id});
            }

            function loadAccounts(endpoint) {
                return accountsListApi.getAccountsList(endpoint)
                    .then(function onAccountsListSuccess(data) {
                        
                        $scope.data = data[0]['current-account'] || [];
                        
                        $log.info($scope.data);

                        if (data.length === 0) {
                            $window.gadgets.pubsub.publish(widget.getPreference('noAccountsEventName'));
                        }
                    }, function onAccountsListError(error) {
                        $log.error('Account list loading failed', error);
              
                });
            }
        }
    ]);

}(window, angular));
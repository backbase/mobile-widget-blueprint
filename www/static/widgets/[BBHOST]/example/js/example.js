(function(window,angular){
	"use strict;"

	var app = angular.module('example', []);

	app.run(function(widget){
		gadgets.pubsub.publish('cxp.item.loaded', {id:widget.model.name});
	})
	
	app.controller('main', ['$scope', 'widget', function($scope, widget) {
		$scope.data = {};
		$scope.data.title = widget.getPreference("title");
		$scope.data.subTitle = 'Hello from Angular';
		
	}]);

}(window,angular));